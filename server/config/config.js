/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                 *
 *    This file is part of shunter-whoami which is released under MIT.                             *
 *    See file 'LICENSE' for full license details.                                                 *
 *                                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// * * * * * * * * Import externals
const dotenv = require('dotenv');
const path = require('path');
const Joi = require('joi');

// * * * * * * * * The code
dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi
      .string()
      .valid('production', 'development', 'test')
      .default('production'),
    SERVER_PORT: Joi
      .number()
      .integer()
      .min(0)
      .max(65535)
      .default(3000),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

// * * * * * * * * Module exports
module.exports = {
  env: envVars.NODE_ENV,
  server: {
    port: envVars.SERVER_PORT
  }
};
