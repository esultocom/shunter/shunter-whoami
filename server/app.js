/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                                 *
 *    This file is part of shunter-whoami which is released under MIT.                             *
 *    See file 'LICENSE' for full license details.                                                 *
 *                                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// * * * * * * * * Import externals
const wantsCheck = require('@esultocom/node-server-middleware-wantscheck');
const ApiError = require('@esultocom/node-server-util-apierror');
const compression = require('compression');
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const httpStatus = require('http-status');
const xss = require('xss-clean');

// * * * * * * * * Import internals
const config = require('./config/config');
const logger = require('./config/logger');
const morgan = require('./config/morgan');
const routes  = require("./routes");
const { errorConverter, errorHandler } = require('./middlewares/error');

// * * * * * * * * The code
var app = express();

if (config.env !== 'test') {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}

// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());

// gzip compression
app.use(compression());

// enable cors
app.use(cors());
app.options('*', cors());

// Custom helper
app.use(wantsCheck);

// Routes
app.set('view engine', 'pug');
app.set('views','./server/views');
app.use(routes);

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

// * * * * * * * * Module exports
module.exports = app;
